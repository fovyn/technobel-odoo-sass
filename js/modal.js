export class Modal {
  #selector;
  #closeBtn;

  /**
   *
   * @param {HtmlElement} selector
   */
  constructor(selector) {
    this.#selector = selector;
    this.#init();
  }

  #init() {
    this.#closeBtn = this.#selector.querySelector(".close");
    this.#closeBtn.addEventListener("click", () => this.close());
  }

  open() {
    this.#selector.style.display = "block";
  }
  close(callback) {
    this.#selector.style.display = "none";
    callback?.call(this.#selector);
  }
}
