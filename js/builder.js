class Formation {
  #formateur;
  #stagiaires = [];

  constructor() {}

  get Formateur() {
    return this.#formateur;
  }
  set Formateur(v) {
    this.#formateur = v;
  }
  get Stagiaires() {
    return this.#stagiaires;
  }

  close() {}

  static builder() {
    return new FormationBuilder();
  }
}

class FormationBuilder {
  #formation;

  constructor() {
    this.#formation = new Formation();
  }

  formateur(v) {
    this.#formation.Formateur = v;
    return this;
  }
  stagiaire(v) {
    this.#formation.Stagiaires.push(v);
    return this;
  }

  build() {
    return this.#formation;
  }
}

const builder = Formation.builder().formateur("Flavian");

builder.stagiaire("Claire").stagiaire("Amaury").stagiaire("Etienne").build();
